import logging

from django.forms.widgets import TextInput
from django.utils.safestring import mark_safe

from djcurrency.utils.numbers import round_decimal

log = logging.getLogger('djcurrency.forms.widgets')


def _render_decimal(value, places=2, min_places=2):

    if value is not None:
        roundfactor = "0." + "0"*(places-1) + "1"
        if value < 0:
            roundfactor = "-" + roundfactor

        value = round_decimal(
            val=value,
            places=places,
            roundfactor=roundfactor,
            normalize=True
        )
        parts = ("%f" % value).split('.')
        n = parts[0]
        d = ""

        if len(parts) > 0:
            d = parts[1]
        elif min_places:
            d = "0" * min_places

        while len(d) < min_places:
            d = "%s0" % d

        while len(d) > min_places and d[-1] == '0':
            d = d[:-1]

        if len(d) > 0:
            value = "%s.%s" % (n, d)
        else:
            value = n
    return value


class BaseCurrencyWidget(TextInput):
    """
    A Text Input widget that shows the currency amount
    """
    def __init__(self, attrs={}):
        final_attrs = {'class': 'vCurrencyField'}
        if attrs is not None:
            final_attrs.update(attrs)
        super(BaseCurrencyWidget, self).__init__(attrs=final_attrs)


class CurrencyWidget(BaseCurrencyWidget):

    def render(self, name, value, attrs=None, renderer=None):
        if value != '':
            try:
                value = _render_decimal(value, places=8)
            except:
                pass
        rendered = super(CurrencyWidget, self).render(name, value, attrs)
        #curr = get_l10n_default_currency_symbol()
        #curr = curr.replace("_", "&nbsp;")
        curr = 'R$'
        return mark_safe('<span class="currency">%s</span>%s' % (
            curr, rendered
        ))
