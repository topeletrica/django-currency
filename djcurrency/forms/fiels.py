from decimal import Decimal

from django import forms
from django.utils.translation import ugettext as _

from djcurrency.utils.numbers import round_decimal, RoundedDecimalError


class RoundedDecimalField(forms.Field):

    def clean(self, value):
        """
        Normalize the field according to cart normalizing rules.
        """
        #cartplaces = config_value('SHOP', 'CART_PRECISION')
        #roundfactor = config_value('SHOP', 'CART_ROUNDING')
        cartplaces = 0
        roundfactor = Decimal(1)

        if not value or value == '':
            value = Decimal(0)

        try:
            value = round_decimal(
                val=value,
                places=cartplaces,
                roundfactor=roundfactor,
                normalize=True,
            )
        except RoundedDecimalError:
            raise forms.ValidationError(
                _('%(value)s is not a valid number') % {'value': value}
            )

        return value


class PositiveRoundedDecimalField(RoundedDecimalField):
    """
    Normalize the field according to cart normalizing rules and force it to be
    positive.
    """
    def clean(self, value):
        value = super(PositiveRoundedDecimalField, self).clean(value)
        if value < 0:
            raise forms.ValidationError(_('Please enter a positive number'))

        return value
