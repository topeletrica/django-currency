from decimal import Decimal, InvalidOperation
import logging
import re

from django.template import Library
from django.utils.safestring import mark_safe
from django.core.exceptions import ImproperlyConfigured

register = Library()

# Create a regex to strip out the decimal places with currency formatting
# Example string = u"$%(val)0.2f" so this regex should let us get the 0.2f
# portion
decimal_fmt = re.compile(r'(\.\d+f)')

log = logging.getLogger('l10n.utils')

# Defined outside the function, so won't be recompiled each time
# moneyfmt is called.
# This is required because some currencies might include a . in the description
decimal_separator = re.compile(r'(\d)\.(\d)')


def moneyfmt(val, currency_code=None, wrapcents='', places=None):
    """
    Formats val according to the currency settings for the desired currency,
    as set in L10N_SETTINGS
    """
    if val is None or val == '':
        val = Decimal('0')

    #currencies = get_l10n_setting('currency_formats')
    currencies = {
        'RBL': {
            'symbol': u'R$',
            'positive': u"R$%(val)0.2f",
            'negative': u"-R$%(val)0.2f",
            'decimal': ','
        },
    }
    currency = None

    if currency_code:
        currency = currencies.get(currency_code, None)
        if not currency:
            log.warn(
                'Could not find currency code definitions for "%s", please '
                'look at l10n.l10n_settings for examples.'
            )

    if not currency:
        #default_currency_code = get_l10n_setting('default_currency', None)
        default_currency_code = 'RBL'

        if not default_currency_code:
            log.fatal("No default currency code set in L10N_SETTINGS")
            raise ImproperlyConfigured(
                "No default currency code set in L10N_SETTINGS"
            )

        if currency_code == default_currency_code:
            raise ImproperlyConfigured(
                "Default currency code '%s' not found in currency_formats "
                "in L10N_SETTINGS",
                currency_code
            )

        return moneyfmt(
            val,
            currency_code=default_currency_code,
            wrapcents=wrapcents,
            places=places,
        )

    # here we are assured we have a currency format

    if val >= 0:
        key = 'positive'
    else:
        val = abs(val)
        key = 'negative'

    # If we've been passed places, modify the format to use the new value
    if places is None or places == '':
        fmt = currency[key]
    else:
        start_fmt = currency[key]
        fmt_parts = re.split(decimal_fmt, start_fmt)
        new_decimal = u".%sf" % places
        # We need to keep track of all 3 parts because we might want to use
        # () to denote a negative value and don't want to lose the trailing )
        fmt = u''.join([fmt_parts[0], new_decimal, fmt_parts[2]])
    formatted = fmt % {'val': val}

    sep = currency.get('decimal', '.')
    if sep != '.':
        formatted = decimal_separator.sub(r'\1%s\2' % sep, formatted)

    if wrapcents:
        pos = formatted.rfind(sep)
        if pos > -1:
            pos += 1
            formatted = u"%s<%s>%s</%s>" % (
                formatted[:pos],
                wrapcents,
                formatted[pos:],
                wrapcents,
            )

    return formatted


def _stripquotes(val):
    stripping = True
    while stripping:
        stripping = False
        if val[0] in ('"', "'"):
            val = val[1:]
            stripping = True
        if val[-1] in ('"', "'"):
            val = val[:-1]
            stripping = True

    return val


def get_filter_args(argstring, keywords=(), intargs=(), boolargs=(),
                    stripquotes=False):
    """Convert a string formatted list of arguments into a kwargs dictionary.
    Automatically converts all keywords in intargs to integers.

    If keywords is not empty, then enforces that only those keywords are
    returned.  Also handles args, which are just elements without an equal sign

    ex:
    in: get_filter_kwargs('length=10,format=medium', ('length'))
    out: (), {'length' : 10, 'format' : 'medium'}
    """
    args = []
    kwargs = {}
    if argstring:
        work = [x.strip() for x in argstring.split(',')]
        work = [x for x in work if x != '']
        for elt in work:
            parts = elt.split('=', 1)
            if len(parts) == 1:
                if stripquotes:
                    elt = _stripquotes(elt)
                args.append(elt)

            else:
                key, val = parts
                val = val.strip()
                if stripquotes and val:
                    val = _stripquotes(val)

                key = key.strip()
                if not key:
                    continue
                key = key.lower().encode('ascii')

                if not keywords or key in keywords:
                    if key in intargs:
                        try:
                            val = int(val)
                        except ValueError:
                            raise ValueError(
                                'Could not convert value "%s" to integer for '
                                'keyword "%s"' % (val, key)
                            )
                    if key in boolargs:
                        val = val.lower()
                        val = val in (1, 't', 'true', 'yes', 'y', 'on')
                    kwargs[key] = val
    return args, kwargs


def currency(value, args=""):
    """Convert a value to a money formatted string.

    places:  required number of places after the decimal point
    curr:    optional currency symbol before the sign (may be blank)
    wrapcents:tag to wrap the part after the decimal point

    Usage:
        val|currency
        val|currency:'places=2'
        val|currency:'places=2:wrapcents=sup'
    """

    if value == '' or value is None:
        return value

    args, kwargs = get_filter_args(
        args,
        keywords=('places', 'curr', 'wrapcents'),
        intargs=('places',),
        stripquotes=True,
    )

    try:
        value = Decimal(str(value))
    except InvalidOperation:
        log.error("Could not convert value '%s' to decimal", value)
        raise

    return mark_safe(moneyfmt(value, **kwargs))

register.filter('currency', currency)
currency.is_safe = True
